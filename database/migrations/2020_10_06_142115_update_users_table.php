<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("users", function (Blueprint $table) {
            $table->renameColumn("name", "firstname");
            $table->text("lastname")->nullable();
            $table->string("avatar")->default("default.png");
            $table->enum("type", ["user", "professional", "expert", "admin"])->default("user");
            $table->text("address")->nullable();
            $table->boolean("email_notification")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists("user");
    }
}
