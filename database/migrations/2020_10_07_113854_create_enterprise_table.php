<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnterpriseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprises', function (Blueprint $table) {
            $table->id();
            $table->text("name");
            $table->text("description");
            $table->enum("type", ["grower", "grocery"]);
            $table->text("city");
            $table->text("address");
            $table->float("latitude")->nullable();
            $table->float("longitude")->nullable();
            $table->integer("owner_id")->unsigned()->nullable();
            $table->foreign("owner_id")->references("id")->on("users")->onDelete("set null");
            $table->boolean("like");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise');
    }
}
