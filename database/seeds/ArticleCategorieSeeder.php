<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\ArticleCategorie;
use App\Models\Article;
use App\Models\Categorie;
use Illuminate\Support\Facades\DB;

class ArticleCategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1,20) as $index) {
            DB::table('article_categories')->insert([
                'article_id' => Article::all()->random()->id,
                'categorie_id' => Categorie::all()->random()->id,
            ]);
        }
    }
}
