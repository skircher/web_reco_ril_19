<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Canard gras',
            'product_category_id' => 1
        ]);
        DB::table('products')->insert([
                    'name' => 'Poulet',
                    'product_category_id' => 1
        ]);
        DB::table('products')->insert([
                    'name' => 'Pintade',
                    'product_category_id' => 1
        ]);
        DB::table('products')->insert([
                    'name' => 'Betteraves',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Blettes',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Céleri rave',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Chou de Milan',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Chou kale',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Chou rave',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Épinards',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Fenouil',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Mâche',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Persil',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Fraises',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Asperges',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Pommes',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Poires',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Raisin',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Noix',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Noisettes',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Châtaignes',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Pommes de terre',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Patates douces',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Salade',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Choux fleur',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Choux rouge',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Poulet',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Poule',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Dinde',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Pintade',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Abondance',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'Tomme Alban',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'Soulère Nature / Poivre / Cumin',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'Soulère Poivre',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'Laguiole',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'Tomme de Brenac',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'Guttus',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'Camendou',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'Bûchettes de vache',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'L’esquerre',
                    'product_category_id' => 4
        ]);
        DB::table('products')->insert([
                    'name' => 'Agneau',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Porc',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Brebis',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Tomates',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Poivrons',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Pommes de terre',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Haricots',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Choux',
                    'product_category_id' => 2
        ]);
        DB::table('products')->insert([
                    'name' => 'Pêches',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Melons',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Abricots',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Kiwis',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Pommes',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Fraises',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Boeuf',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Veau',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Agneau',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => '',
                    'product_category_id' => 5
        ]);
        DB::table('products')->insert([
                    'name' => 'Tomates',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Poivrons',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Piments',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Courgettes',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Aubergines',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Framboises',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Artichauts',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Rhubarbes',
                    'product_category_id' => 3
        ]);
        DB::table('products')->insert([
                    'name' => 'Salade',
                    'product_category_id' => 2
        ]);
    }
}
