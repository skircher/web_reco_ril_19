<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Categorie;
use Illuminate\Support\Facades\DB;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * 
     * 
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Conseils',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('categories')->insert([
            'name' => 'Cuisine',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('categories')->insert([
            'name' => 'Ecologie',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('categories')->insert([
            'name' => 'Economie',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('categories')->insert([
            'name' => 'Politique',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('categories')->insert([
            'name' => 'Santé',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
        DB::table('categories')->insert([
            'name' => 'Société',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
