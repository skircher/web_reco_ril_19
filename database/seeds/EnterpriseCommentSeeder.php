<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EnterpriseComment;

class EnterpriseCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EnterpriseComment::factory()
                        ->times(100 * 10)
                        ->create();
    }
}
