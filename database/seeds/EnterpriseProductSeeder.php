<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\EnterpriseProduct;

class EnterpriseProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EnterpriseProduct::factory()
                    ->times(40)
                    ->create();
    }
}
