<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            CategorieSeeder::class,
            ArticleSeeder::class,
            EnterpriseSeeder::class,
            ProductCategorySeeder::class,
            ProductSeeder::class,
            EnterpriseProductSeeder::class,
            EnterpriseCommentSeeder::class,
            EnterpriseReviewTypeSeeder::class,
            EnterpriseUserReviewSeeder::class,
            ArticleCategorieSeeder::class,
        ]);
    }
}
