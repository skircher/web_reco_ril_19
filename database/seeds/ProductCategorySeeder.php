<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\DB;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_categories')->insert([
            'name' => "Volaille",
            'icon' => "kiwi-bird"
        ]);

        DB::table('product_categories')->insert([
            'name' => "Légumes",
            'icon' => "carrot"
        ]);

        DB::table('product_categories')->insert([
            'name' => "Fruits",
            'icon' => "apple-alt"
        ]);

        DB::table('product_categories')->insert([
            'name' => "Laitiers",
            'icon' => "cheese"
        ]);

        DB::table('product_categories')->insert([
            'name' => "Viandes",
            'icon' => "drumstick-bite"
        ]);
    }
}
