<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\EnterpriseUserReview;

class EnterpriseUserReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EnterpriseUserReview::factory()
                            ->times(100)
                            ->create();
    }
}
