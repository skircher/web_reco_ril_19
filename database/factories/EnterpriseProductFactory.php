<?php

namespace Database\Factories;

use App\Models\EnterpriseProduct;
use App\Models\Enterprise;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnterpriseProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EnterpriseProduct::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'enterprise_id' => Enterprise::all()->random()->id,
            'product_id' => Product::all()->random()->id
        ];
    }
}
