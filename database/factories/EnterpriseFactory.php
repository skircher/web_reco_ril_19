<?php

namespace Database\Factories;

use App\Models\Enterprise;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnterpriseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Enterprise::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company,
            'description' => $this->faker->text(1000),
            'type' => $this->faker->randomElement(["grower", "grocery"]),
            'city' => $this->faker->randomElement(["Toulouse", "Montauban", "Albi", "Castanet-Tolosan", "Villefranche"]),
            'address' => $this->faker->address,
            'latitude' => $this->faker->randomFloat(NULL, 42.646273, 43.950364),
            'longitude' => $this->faker->randomFloat(NULL, 0.404494, 2.05577),
            'owner_id' => User::all()->random()->id,
            'like' => $this->faker->boolean(10)
        ];
    }
}
