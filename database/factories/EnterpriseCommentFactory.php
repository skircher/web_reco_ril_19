<?php

namespace Database\Factories;

use App\Models\EnterpriseComment;
use App\Models\Enterprise;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnterpriseCommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EnterpriseComment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'enterprise_id' => Enterprise::all()->random()->id,
            'user_id' => User::all()->random()->id,
            'message' => $this->faker->text(500)
        ];
    }
}
