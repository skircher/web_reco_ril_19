<?php

namespace Database\Factories;

use App\Models\EnterpriseUserReview;
use App\Models\Enterprise;
use App\Models\User;
use App\Models\EnterpriseReviewType;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnterpriseUserReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EnterpriseUserReview::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'enterprise_id' => Enterprise::all()->random()->id,
            'user_id' => User::all()->random()->id,
            'type_id' => EnterpriseReviewType::all()->random()->id,
            'review' => $this->faker->biasedNumberBetween(0, 5)
        ];
    }
}
