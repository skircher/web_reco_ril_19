<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        ArticleCategorie::class => ArticleCategoriePolicy::class,
        Article::class => ArticlePolicy::class,
        Categorie::class => CategoriePolicy::class,
        EnterpriseComment::class => EnterpriseCommentPolicy::class,
        Enterprise::class => EnterprisePolicy::class,
        EnterpriseProduct::class => EnterpriseProductPolicy::class,
        EnterpriseReviewType::class => EnterpriseReviewTypePolicy::class,
        ProductCategory::class => ProductCategoryPolicy::class,
        Product::class => ProductPolicy::class,
        User::class => UserPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
