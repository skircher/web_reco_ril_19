<?php

namespace App\Policies;

use App\Models\EnterpriseComment;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EnterpriseCommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(?User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\EnterpriseComment  $enterpriseComment
     * @return mixed
     */
    public function view(?User $user, EnterpriseComment $enterpriseComment)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\EnterpriseComment  $enterpriseComment
     * @return mixed
     */
    public function update(User $user, EnterpriseComment $enterpriseComment)
    {
        return $user->isAdministrator() || $user == $enterpriseComment->user;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\EnterpriseComment  $enterpriseComment
     * @return mixed
     */
    public function delete(User $user, EnterpriseComment $enterpriseComment)
    {
        return $user->isAdministrator();
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\EnterpriseComment  $enterpriseComment
     * @return mixed
     */
    public function restore(User $user, EnterpriseComment $enterpriseComment)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\EnterpriseComment  $enterpriseComment
     * @return mixed
     */
    public function forceDelete(User $user, EnterpriseComment $enterpriseComment)
    {
        return $user->isAdministrator();
    }
}
