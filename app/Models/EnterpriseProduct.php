<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EnterpriseProduct extends Model
{
    use HasFactory;

    /**
     * Indicates model primary keys.
     */
    protected $primaryKey = ['enterprise_id', 'product_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['enterpise_id', 'product_id'];

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
        'product_category'
    ];

    public function category() {
        return $this->belongsTo(ProductCategory::class);
    }

    public $incrementing = false;

    public $timestamps = false;
}
