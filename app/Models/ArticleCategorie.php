<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleCategorie extends Model
{
    use HasFactory;

    /**
     * Indicates model primary keys.
     */
    protected $primaryKey = ['article_id', 'categorie_id'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id', 'categorie_id'
    ];

    public $incrementing = false;

    public $timestamps = false;
}