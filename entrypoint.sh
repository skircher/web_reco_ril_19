#!/bin/sh

if [[ ! -z $APP_DEV ]] || [[ ! -z $APP_TEST ]]; then
    composer install
    php artisan config:cache
    php artisan route:cache
    php artisan view:cache
fi

php artisan migrate

exec "$@"