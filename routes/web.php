<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("storage/enterprises/{filename}", function ($filename)
{
    $path = storage_path("app/enterprises/".$filename);

    if (!File::exists($path)) {
        $path = storage_path("app/enterprises/default.jpg");
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get("storage/avatars/{filename}", function ($filename)
{
    $path = storage_path("app/avatars/".$filename);

    if (!File::exists($path)) {
        $path = storage_path("app/avatars/default.png");
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/{any}', function() {
    return view('index');
})->where('any', '.*');