<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\ArticleController;
use App\Http\Controllers\API\EnterpriseReviewTypeController;
use App\Http\Controllers\API\EnterpriseController;
use App\Http\Controllers\API\EnterpriseProductController;
use App\Http\Controllers\API\EnterpriseCommentController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\ProductCategoryController;
use App\Http\Controllers\API\ArticleCategorieController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Auth::routes();

Route::get('user', [UserController::class, "user"])->middleware('auth');
Route::get('user/enterprises', [UserController::class, "enterprises"])->middleware('auth');

Route::post("enterprises/bounds", [EnterpriseController::class, "getEnterprisesByBounds"]);

Route::get("users/search", [UserController::class, "search"]);
Route::apiResource("users", API\UserController::class);
Route::apiResource("article.categorie", API\ArticleCategorieController::class);
Route::get("categorie/{categorie}/article/", [ArticleCategorieController::class, "listBycategorie"]);
Route::get("articles/search", [ArticleController::class, "search"]);
Route::apiResource("articles", API\ArticleController::class);
Route::apiResource("enterprise/reviewtypes", API\EnterpriseReviewTypeController::class);
Route::get("enterprises/search", [EnterpriseController::class, "search"]);
Route::get("enterprises/filters", [EnterpriseController::class, "filters"]);
Route::apiResource("enterprises", API\EnterpriseController::class);
Route::apiResource("enterprises.products", API\EnterpriseProductController::class);
Route::get("enterprises/{enterprise}/comments/search", [EnterpriseCommentController::class, "search"]);
Route::get("enterprises/{enterprise}/comments/hascomment", [EnterpriseCommentController::class, "has_comment"]);
Route::apiResource("enterprises.comments", API\EnterpriseCommentController::class);
Route::get("product/categories/search", [ProductCategoryController::class, "search"]);
Route::apiResource("product/categories", API\ProductCategoryController::class);
Route::get("products/search", [ProductController::class, "search"]);
Route::apiResource("products", API\ProductController::class);

Route::apiResource("article_categories", API\CategorieController::class);

Route::any("/{any}", function() {
    return response(["error" => "Not found"], 404);
});