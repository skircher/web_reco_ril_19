<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Locavor</title>
    </head>
    <body>
        <h1>Nouveau commentaire</h1>
        <h2>{{ $userName }}</h2>

        <span>{{ $comment }}</span>
    </body>
</html>