/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

const { default: VueRouter } = require('vue-router');
const { default: BootstrapVue, BootstrapVueIcons } = require('bootstrap-vue');

require('./bootstrap');

window.Vue = require('vue');

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(VueLuxon);

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faHome, faCarrot, faAppleAlt, faCheese, faLeaf, faCrown, faDove, faDrumstickBite, faKiwiBird } from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faInstagram, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons'
import VueLuxon from "vue-luxon";

library.add(faFacebook, faInstagram, faTwitter, faYoutube, faHome, faCarrot, faAppleAlt, faCheese, faLeaf, faCrown, faDove, faDrumstickBite, faKiwiBird)

Vue.component('font-awesome-icon', FontAwesomeIcon)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('navbar', require('./components/Navbar.vue').default);
Vue.component('app-footer', require('./components/Footer.vue').default);
Vue.component('osm-map', require('./components/OsmMap.vue').default);
Vue.component('signin-mod', require('./components/SigninMod.vue').default);
Vue.component('register-mod', require('./components/RegisterMod.vue').default);
Vue.component('editProfile-mod', require('./components/editProfileMod.vue').default);
Vue.component('deleteProfile-mod', require('./components/deleteProfileMod.vue').default);
Vue.component('updatePwd-mod', require('./components/updatePwdMod.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**
 * Filters
 */

Vue.filter("formatDate", function (value) {
    if (value)
        return (new Date(value)).toLocaleString()
})

Vue.filter("round", function(value) {
    if(value)
        return value.toFixed(2)
})

/**
 * Routes
 */

const routes = [
    {
        path: '/',
        name: 'home',
        component: require('./views/HomePage.vue').default,
        children: [
            {
                path: 'home',
                component: require('./views/HomePage.vue').default
            },
        ]
    },
    {
        path: '/locavor-c-est-quoi',
        name: 'locavor-c-est-quoi',
        component: require('./views/LocavorCestQuoi.vue').default
    },
    {
        path: '/producteurs-toulousain',
        name: 'producteurs-toulousain',
        component: require('./views/ProducteursToulousain.vue').default
    },
    {
        path: '/producteur/:id',
        name: 'producteur',
        component: require('./views/Producteur.vue').default
    },
    {
        path: '/producteur/:id/edit',
        name: 'edit_producteur',
        component: require('./views/EditProducteur.vue').default
    },
    {
        path: '/epiceries-locales',
        name: 'epiceries-locales',
        component: require('./views/EpiceriesLocales.vue').default
    },
    {
        path: '/news',
        name: 'news',
        component: require('./views/News.vue').default
    },
    {
        path: '/administration/users',
        name: 'administration_users',
        component: require('./views/administration/Users.vue').default
    },
    {
        path: '/administration/articles',
        name: 'administration_articles',
        component: require('./views/administration/Articles.vue').default
    },
    {
        path: '/administration/enterprises',
        name: 'administration_enterprises',
        component: require('./views/administration/Enterprises.vue').default
    },
    {
        path: '/administration/product/categories',
        name: 'administration_product_categories',
        component: require('./views/administration/ProductCategories.vue').default
    },
    {
        path: '/administration/products',
        name: 'administration_products',
        component: require('./views/administration/Products.vue').default
    },
    {
        path: '/apropos',
        name: 'a-propos',
        component: require('./views/Apropos.vue').default
    },
    {
        path: '/profile/:id',
        name: 'user-profile',
        component: require('./views/userProfile.vue').default,
        /* meta: { authOnly: true } */
    },

    {
        path: "*",
        component: require('./views/NotFound.vue').default
    }
]

const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: routes
});

/* function isLoggedIn() {
    return localStorage.getItem("token");
}


router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.authOnly)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!isLoggedIn()) {
            next({
                path: '/home',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
}) */


const app = new Vue({
    router,
    data: {
        authUser: null,
        dataLoaded: false
    },
    mounted() {
        this.auth();
    },
    methods: {
        auth() {
            axios
                .get("/api/user")
                .then((response) => {
                    this.authUser = response.data;
                    this.dataLoaded = true;
                })
                .catch((error) => {
                    const errorResponse = error.response.data.errors;
                    this.dataLoaded = true;
                });
        },
        isAuthenticated() {
            return this.authUser != null
        },
        isSelf(user) {
            return this.authUser && user && this.authUser.id == user.id
        },
        isExpert() {
            return this.authUser && this.authUser.type == "expert"
        },
        isAdministrator() {
            return this.authUser && this.authUser.type == "admin"
        }
    }
}).$mount('#app');
